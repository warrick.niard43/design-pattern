<?php

class Voiture
{

    private static $couleur = "Rouge";
    private static $puissance = 130;
    private static $vitesse = null;

    private function __construct() {  
    }
  
    /**
     * Méthode qui crée l'unique instance de la classe
     * si elle n'existe pas encore puis la retourne.
     *
     * @param void
     * @return Voiture
     */
    public static function getVitesse() {
  
      if(is_null(self::$vitesse)) {
        self::$vitesse = new Voiture();  
      }
  
      return self::$vitesse;
    }

    public static function getPuissance() {
  
      if(is_null(self::$puissance)) {
        self::$puissance = new Voiture();  
      }
  
      return self::$puissance;
    }

    public static function getCouleur() {
  
      if(is_null(self::$couleur)) {
        self::$couleur = new Voiture();  
      }
  
      return self::$couleur;
    }
};

