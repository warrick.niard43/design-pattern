<?php

 
  // Import de la classe
  require(dirname(__FILE__).'/classeA.php');
  
  // Tentative d'instanciation de la classe
  $VoitureA = Voiture::getCouleur();
  $VoitureB = Voiture::getPuissance();
  $VoitureC = Voiture::getVitesse();
  
  echo '<pre>';
  var_dump($VoitureA);
  echo '</pre>';
  
  echo '<pre>';
  var_dump($VoitureB);
  echo '</pre>';
  
  echo '<pre>';
  var_dump($VoitureC);
  echo '</pre>';
  
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////

  require(dirname(__FILE__).'/Factory.php');

  $item=["Blablabla"];

  $Factory = Factory::create($item);
  
  echo '<pre>';
  var_dump($Factory);
  echo '</pre>';
?>