<?php



class Factory
{

     private static $choice = null;

    public static function create($item)
    {
        if ( is_array($item)) {         // FACTORY
            self::$choice = MongoDB::setDatabase($item);
            return self::$choice;
        } else {
            self::$choice = MySQL::setDatabase($item);
            return self::$choice;
        }
    }
}

class MySQL
{
    private static $instance = null;
    private static $mysql = "MySQL :";

    private static $database = [];
    public static function setDatabase($item)
    {
        if ( is_null(self::$instance)) {         // SINGLETON
            self::$instance = new MySQL();
            return self::$database = [self::$mysql . $item];
        }
        
    }
}

class MongoDB
{
    private static $instance = null;
    private static $mongo = "MongoDB :";

    private static $database = [];
    public static function setDatabase($item)
    {
        if ( is_null(self::$instance)) {         // SINGLETON
            self::$instance = new MongoDB();
            return self::$database = [self::$mongo, $item];
        }
    }
}
